package services;

import dao.CustomerDao;
import entity.Beer;
import entity.Customer;

import java.util.List;

public class CustomerService {
    private CustomerDao customersDao = new CustomerDao();

    public CustomerService() {
    }

    public Customer findCustomer(int id) {
        return customersDao.findById(id);
    }

    public void saveCustomer(Customer customer) {
        customersDao.save(customer);
    }

    public void deleteCustomer(Customer customer) {
        customersDao.delete(customer);
    }

    public void updateCustomer(Customer customer) {
        customersDao.update(customer);
    }

    public List<Customer> findAllUsers() {
        return customersDao.findAll();
    }

    public Beer findBeerById(int id) {
        return customersDao.findAutoById(id);
    }
}
