import entity.Beer;
import entity.Customer;
import services.CustomerService;

public class Main {
    public static void main(String[] args) {
        CustomerService customerService = new CustomerService();
        Customer customer = new Customer("Masha",26);
        customerService.saveCustomer(customer);
        Beer alivaria = new Beer("Alivaria", "Belarus",15);
        alivaria.setCustomer(customer);
        customer.addBeer(alivaria);
        Beer heineken = new Beer("Heineken","Denmark",20);
        heineken.setCustomer(customer);
        customerService.updateCustomer(customer);
    }
}
